package com.cooperative.eshop.module.Sale;

import com.cooperative.eshop.module.Member.MemberService;
import com.cooperative.eshop.module.Offer.Offer;
import com.cooperative.eshop.module.Offer.OfferService;
import com.cooperative.eshop.module.Payment.PaymentService;
import com.cooperative.eshop.module.Cart.Cart;
import com.cooperative.eshop.module.Payment.Payment;
import java.time.LocalDateTime;
import java.util.List;
import com.cooperative.eshop.module.Cart.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SaleServiceImpl implements SaleService{
    
    @Autowired
    private SaleDAO saleDAO;
    @Autowired
    private CartService cartService;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private OfferService offerService;
    

    @Transactional
    @Override
    public List<Sale> getSales() {
        return saleDAO.getSales();
    }

    @Transactional
    @Override
    public void saveSale(Sale theSale) {
        saleDAO.saveSale(theSale);
    }

    @Transactional
    @Override
    public Sale getSale(int theId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional
    @Override
    public void deleteSale(int theId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



    @Transactional
    @Override
    public List<Sale> getSalesByDate(int monthsBefore) {
        
        return saleDAO.getSalesByDate(
                LocalDateTime.now().minusMonths(monthsBefore),
                (monthsBefore==1?LocalDateTime.now():LocalDateTime.now().minusMonths(monthsBefore-1)));
        
    }

    @Transactional
    @Override
    public void orderWholeCart(List<Cart> cart) {



        for (Cart cartProduct:cart){

            List<Offer> productOffers = offerService.getActiveOffers(cartProduct.getProduct());
            int offerNumber = productOffers.size();

            final double CURRENT_PRODUCT_PRICE_SHOP = cartProduct.getProduct().getPrice_shop();
            final double SALE_PROFIT = CURRENT_PRODUCT_PRICE_SHOP * cartProduct.getQuantity();
            final double ADMIN_SALE_PROFIT = SALE_PROFIT / 2 ;
            final double MEMBERS_SUM_PROFIT = SALE_PROFIT - ADMIN_SALE_PROFIT ;
            final double MEMBER_SALE_PROFIT = MEMBERS_SUM_PROFIT / offerNumber ;
            double remaining = MEMBERS_SUM_PROFIT;


            Sale sale = new Sale(
                    cartProduct.getProduct(),
                    cartProduct.getCustomer(),
                    CURRENT_PRODUCT_PRICE_SHOP,
                    cartProduct.getQuantity()
            );
            
            saleDAO.saveSale(sale);

            //The payment of admin(50%)
            //TODO : the above percentage of the admin payment should be selectable by the admin
            Payment adminPayment = new Payment(sale, memberService.getMember(1), ADMIN_SALE_PROFIT);
            paymentService.savePayment(adminPayment);

            //Split the Members profit to the current members that has active offers for product selling.
            for (Offer offer : productOffers){
                Payment memberPayment = new Payment(sale, offer.getMember(), MEMBER_SALE_PROFIT);
                paymentService.savePayment(memberPayment);
            }

            cartService.deleteCartProduct(cartProduct.getId());

        }
        
    }

    @Transactional
    @Override
    public Double getSalesSum() {
        Double theSum = 0.00;
        List<Object[]> list = saleDAO.getSaleQuantityAndPrice();
        for (Object[] o: list){
            theSum += ((Integer)o[0] * (Double)o[1]);
        }
        return theSum;
    }

    @Transactional
    @Override
    public List<Sale> getSalesSortedBy(String ssb, String osb) {
        return  saleDAO.getSalesSortedBy(ssb, osb);
    }


}

package com.cooperative.eshop.module.Product;

import java.util.List;

public interface ProductDAO {
    
    List<Product> getProducts();

    List<Product> getProductsScrollable();

    List<Product> getProductsSortedBy(String sb, String or);

    void saveProduct(Product theProduct);

    Product getProduct(int theId);

    void deleteProduct(int theId);

    List<Product> getProductsByCategory(String theCategory);
}

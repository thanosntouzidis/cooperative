package com.cooperative.eshop.module.Payment;

import java.util.List;


/**
 *
 * @author Athan
 */

public interface PaymentService {

    void savePayment(Payment payment);

    List<Payment> getPayments();

    Double getAdminProfit();

    Double getMembersProfit();

    Double getPersonalProfit(int memberId);

    List<Payment> getPaymentsSortedBy(String spab, String opab);
}

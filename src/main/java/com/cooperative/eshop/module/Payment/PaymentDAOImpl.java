package com.cooperative.eshop.module.Payment;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author Athan
 */
@Repository
public class PaymentDAOImpl implements PaymentDAO{
    
    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public List<Payment> getPaymentsSortedBy(String spab, String opab) {
        Session ses = sessionFactory.getCurrentSession();
        String hql = "from Payment order by " + spab + " " + opab;
        Query<Payment> thequery = ses.createQuery(hql, Payment.class);
        return thequery.getResultList();
    }

    @Override
    public void savePayment(Payment thePayment) {
        Session ses = sessionFactory.getCurrentSession();
        ses.saveOrUpdate(thePayment);
    }

    @Override
    public List<Payment> getPayments() {
        Session ses = sessionFactory.getCurrentSession();
        Query<Payment> thequery = ses.createQuery("from Payment order by id", Payment.class);
        List<Payment> payments = thequery.getResultList();
        return payments;
    }

    @Override
    public List<Object> getAdminPaymentAmounts() {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("select payment.amount from Payment payment where payment.member=1", Object.class);
        return thequery.getResultList();
    }

    @Override
    public List<Object> getMembersPaymentAmounts() {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("select payment.amount from Payment payment where payment.member!=1", Object.class);
        return thequery.getResultList();
    }

    @Override
    public List<Object> getPersonalPaymentAmounts(int memberId) {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("select payment.amount from Payment payment where payment.member.id= :memberId", Object.class);
        thequery.setParameter("memberId", memberId);
        return thequery.getResultList();
    }



}

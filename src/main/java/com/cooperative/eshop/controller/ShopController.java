package com.cooperative.eshop.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

import com.cooperative.eshop.module.Category.Category;
import com.cooperative.eshop.module.Category.CategoryService;
import com.cooperative.eshop.module.Product.Product;
import com.cooperative.eshop.module.Product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller

public class ShopController {
    
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    
    @RequestMapping("/")
    public String showWelcomePage(HttpSession session){
        List<Product> cart = new ArrayList<>();
        session.setAttribute("cart", cart);
        return "welcome";
    }
    
    @GetMapping("/shop")
    public String showShop(Model model){
        List<Product> products = productService.getProducts();
        //TODO: to replace with the new method
        List<Product> fruits = productService.getProductsByCategory("fruits");
        List<Product> vegetables = productService.getProductsByCategory("vegetables");
        List<Product> mushrooms = productService.getProductsByCategory("mushrooms");
        List<Category> categories = categoryService.getCategories();
        model.addAttribute("products", products);
        model.addAttribute("fruits", fruits);
        model.addAttribute("vegetables", vegetables);
        model.addAttribute("mushrooms", mushrooms);
        model.addAttribute("categories", categories);
        return "shop";
    }
    
    @GetMapping("/shop/productDetails")
    public String showProductDetails(@RequestParam("productId") int theId, Model model){
//        List<Product> mushrooms = productService.getProductsByCategoryName("mushrooms");
        model.addAttribute("product", productService.getProduct(theId));
        model.addAttribute("theCategories", categoryService.getCategories());
        return "product-buy-form";
    }
    
    @PostMapping("/shop/productDetails")
    public String addProductToCart(@RequestParam("productId") int theId, Model model){
        model.addAttribute("product", productService.getProduct(theId));
        model.addAttribute("addSuccessfully", "Product added to cart");
        return "product-buy-form";
    }
}
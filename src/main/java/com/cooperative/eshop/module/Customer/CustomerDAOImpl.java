
package com.cooperative.eshop.module.Customer;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDAOImpl implements CustomerDAO{

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public List<Customer> getCustomers(){
        //get the current hibernate session
        Session ses = sessionFactory.getCurrentSession();
        
        //create a query
        Query<Customer> thequery = ses.createQuery("from Customer order by id", Customer.class);
        
        //execute query and get result list
        List<Customer> customers = thequery.getResultList();
        
        return customers; 
    }

    @Override
    public List<Customer> getCustomersSortedBy(String scb, String ocb) {
        Session ses = sessionFactory.getCurrentSession();
        String hql = "from Customer order by " + scb + " " + ocb;
        Query<Customer> thequery = ses.createQuery(hql, Customer.class);
        return thequery.getResultList();
    }

    @Override
    public void saveCustomer(Customer theCustomer) {
        Session ses = sessionFactory.getCurrentSession();
        ses.saveOrUpdate(theCustomer);
    }

    @Override
    public Customer getCustomer(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        Customer theCustomer = ses.get(Customer.class, theId);
        return theCustomer;
    }

    @Override
    public void deleteCustomer(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("delete from Customer where id=:customerId");
        thequery.setParameter("customerId", theId);
        thequery.executeUpdate();
    }

    @Override
    public List<Customer> searchCustomers(String theSearchName) {
        Session ses = sessionFactory.getCurrentSession();
        Query theQuery;
        if(theSearchName != null && theSearchName.trim().length()>0){
            theQuery = ses.createQuery("from Customer where lower(firstName) like :theName or lower(lastName) like :theName", Customer.class);
            theQuery.setParameter("theName","%"+theSearchName.toLowerCase()+"%");
        } else {
            theQuery = ses.createQuery("from Customer", Customer.class);
        }
        List<Customer> customers = theQuery.getResultList();
        return customers;
    }

    @Override
    public Customer getCustomerByUsername(String theUsername) {
        Session ses = sessionFactory.getCurrentSession();
        Query theQuery = ses.createQuery("from Customer where lower(username) like :theName", Customer.class);
        theQuery.setParameter("theName",theUsername.toLowerCase());
        
        Customer theCustomer = (Customer)theQuery.getSingleResult();
        return theCustomer;
    }

    @Override
    public void disable(int theId) {
        Session ses = sessionFactory.getCurrentSession();
//        Query thequery = ses.createQuery("update Offer set active=0 where id=:offerId");
//        thequery.setParameter("offerId", theId);
//        thequery.executeUpdate();
    }


}

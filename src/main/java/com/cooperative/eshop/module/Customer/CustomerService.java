
package com.cooperative.eshop.module.Customer;

import java.util.List;

public interface CustomerService {
    
    List<Customer> getCustomers();
    
    void saveCustomer(Customer theCustomer);

    Customer getCustomer(int theId);
    
    Customer getCustomerByUsername(String theUsername);

    void deleteCustomer(int theId);

    List<Customer> searchCustomers(String theSearchName);

    void disable(int theId);

    List<Customer> getCustomersSortedBy(String scb, String ocb);
}

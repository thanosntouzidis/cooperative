package com.cooperative.eshop.module.Product;

import java.util.List;

public interface ProductService {
    
    List<Product> getProducts();

    List<Product> getProductsScrollable();

    List<Product> getProductsByCategory(String theCategory);

    List<Product> getProductsSortedBy(String sb, String or);
    
    void saveProduct(Product theProduct);

    Product getProduct(int theId);

    void deleteProduct(int theId);

}

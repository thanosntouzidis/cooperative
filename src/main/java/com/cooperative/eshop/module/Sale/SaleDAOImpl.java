
package com.cooperative.eshop.module.Sale;

import java.time.LocalDateTime;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SaleDAOImpl implements SaleDAO{
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Sale> getSales() {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("from Sale order by id", Sale.class);
        List<Sale> sales = thequery.getResultList();
        return sales;
    }

    @Override
    public List<Sale> getSalesSortedBy(String ssb, String osb) {
        Session ses = sessionFactory.getCurrentSession();
        String hql = "from Sale order by " + ssb + " " + osb;
        Query<Sale> thequery = ses.createQuery(hql, Sale.class);
        return thequery.getResultList();
    }

    @Override
    public void saveSale(Sale theSale) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(theSale);
    }

    @Override
    public Sale getSale(int theId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    //Its not get used. Map is not a solution. map keys are uniques...
    @Override
    public List<Object[]> getSaleQuantityAndPrice() {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("select sale.quantity, sale.price from Sale sale", Object[].class);
        return thequery.getResultList();
    }

    @Override
    public List<Sale> getSalesByDate(LocalDateTime stDate, LocalDateTime edDate) {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("from Sale sale where sale.dateOfC between :stDate and :edDate", Object[].class);
        thequery.setParameter("stDate", stDate);
        thequery.setParameter("edDate", edDate);
        return thequery.getResultList();
    }
    
}

package com.cooperative.eshop.module.Offer;

import java.util.List;

import com.cooperative.eshop.module.Product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OfferServiceImpl implements OfferService{

    @Autowired
    private OfferDAO offerDAO;
    
    @Transactional
    @Override
    public List<Offer> getOffers() {
        return offerDAO.getOffers();
    }

    @Transactional
    @Override
    public List<Offer> getActiveOffers(Product product) { return offerDAO.getActiveOffers(product); }

    @Transactional
    @Override
    public List<Offer> getOffersSortedBy(String sob, String oob) {
        return offerDAO.getOffersSortedBy(sob, oob);
    }

    @Transactional
    @Override
    public void saveOffer(Offer theOffer) {
        offerDAO.saveOffer(theOffer);
    }

    @Transactional
    @Override
    public Offer getOffer(int theId) {
        return offerDAO.getOffer(theId);
    }

    @Transactional
    @Override
    public void deleteOffer(int theId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional
    @Override
    public void activate(int theId) {
     offerDAO.activate(theId);   
    }
    
    @Transactional
    @Override
    public void deactivate(int theId) {
     offerDAO.deactivate(theId);   
    }



}

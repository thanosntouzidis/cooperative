
package com.cooperative.eshop.module.Customer;

import java.util.List;

public interface CustomerDAO {
    
    List<Customer> getCustomers();

    void saveCustomer(Customer theCustomer);

    Customer getCustomer(int theId);

    void deleteCustomer(int theId);

    List<Customer> searchCustomers(String theSearchName);

    Customer getCustomerByUsername(String theUsername);

    void disable(int theId);

    List<Customer> getCustomersSortedBy(String scb, String ocb);
}

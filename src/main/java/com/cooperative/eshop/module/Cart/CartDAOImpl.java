package com.cooperative.eshop.module.Cart;

import java.util.List;

import com.cooperative.eshop.module.Customer.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CartDAOImpl implements CartDAO{

    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Override
    public Cart getCartProduct(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        return ses.get(Cart.class, theId);
    }

    @Override
    public List<Cart> getProductsOfCart(Customer theCustomer) {
        Session ses = sessionFactory.getCurrentSession();
        Query<Cart> thequery = ses.createQuery("from Cart as cart where cart.customer=:customer", Cart.class);
        thequery.setParameter("customer", theCustomer);
        List<Cart> cart = thequery.getResultList();
        return cart;
    }

    @Override
    public void saveProductToCart(Cart theCart) {
        Session ses = sessionFactory.getCurrentSession();
        ses.saveOrUpdate(theCart);
    }

    @Override
    public void deleteCartProduct(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("delete from Cart where id=:cartProductId");
        thequery.setParameter("cartProductId", theId);
        thequery.executeUpdate();
    }
    
}

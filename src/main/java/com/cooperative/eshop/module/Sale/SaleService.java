package com.cooperative.eshop.module.Sale;

import com.cooperative.eshop.module.Cart.Cart;
import java.util.List;


public interface SaleService {
    
    List<Sale> getSales();
    
    void saveSale(Sale theSale);

    Sale getSale(int theId);

    void deleteSale(int theId);

    List<Sale> getSalesByDate(int monthsBefore);

    void orderWholeCart(List<Cart> cart);

    Double getSalesSum();

    List<Sale> getSalesSortedBy(String ssb, String osb);
}

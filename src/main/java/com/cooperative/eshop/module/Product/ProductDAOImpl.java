package com.cooperative.eshop.module.Product;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ProductDAOImpl implements ProductDAO{

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public List<Product> getProducts(){
        //get the current hibernate session
        Session ses = sessionFactory.getCurrentSession();
        //create a query
        Query<Product> thequery = ses.createQuery("from Product order by name", Product.class);
        //execute query and get result list
        List<Product> products = thequery.getResultList();
        return products; 
    }

    @Transactional
    @Override
    public List<Product> getProductsScrollable() {
        Session ses = sessionFactory.getCurrentSession();
        String hql = "FROM Product p order by p.name";
        Query query = ses.createQuery(hql);
        int pageSize = 10;

        ScrollableResults resultScroll = query.scroll(ScrollMode.FORWARD_ONLY);
        resultScroll.first();
        resultScroll.scroll(10);
        List<Product> productPage = new ArrayList<>();
        int i = 0;
        while (pageSize > i++) {
            productPage.add((Product) resultScroll.get(0));
            if (!resultScroll.next())
                break;
        }

        return productPage;
    }

    @Override
    public List<Product> getProductsSortedBy(String sb, String or) {
        Session ses = sessionFactory.getCurrentSession();
        String hql = "from Product order by " + sb + " " + or;
        Query<Product> thequery = ses.createQuery(hql, Product.class);
        return thequery.getResultList();
    }

    @Override
    public void saveProduct(Product theProduct) {
        Session ses = sessionFactory.getCurrentSession();
        ses.saveOrUpdate(theProduct);
    }

    @Override
    public Product getProduct(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        Product theProduct = ses.get(Product.class, theId);
        return theProduct;
    }

    @Override
    public void deleteProduct(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("delete from Product where id=:productId");
        thequery.setParameter("productId", theId);
        thequery.executeUpdate();
    }

    @Override
    public List<Product> getProductsByCategory(String theCategory) {
        Session ses = sessionFactory.getCurrentSession();
        Query theQuery = ses.createQuery("from Product where category like :theCategoryName", Product.class);
        theQuery.setParameter("theCategoryName",theCategory);
        return theQuery.getResultList();
    }

}

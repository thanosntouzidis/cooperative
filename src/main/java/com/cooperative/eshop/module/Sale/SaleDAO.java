
package com.cooperative.eshop.module.Sale;

import java.time.LocalDateTime;
import java.util.List;

public interface SaleDAO {
    
    List<Sale> getSales();
    
    List<Sale> getSalesByDate(LocalDateTime stDate, LocalDateTime edDate);

    void saveSale(Sale theSale);

    Sale getSale(int theId);
    
    List getSaleQuantityAndPrice();


    List<Sale> getSalesSortedBy(String ssb, String osb);
}

package com.cooperative.eshop.module.Offer;

import java.util.List;

import com.cooperative.eshop.module.Product.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OfferDAOImpl implements OfferDAO{
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Offer> getOffers() {
        Session ses = sessionFactory.getCurrentSession();
        Query<Offer> thequery = ses.createQuery("from Offer order by id", Offer.class);
        List<Offer> offers = thequery.getResultList();
        return offers;
    }

    @Override
    public List<Offer> getOffersSortedBy(String sob, String oob) {
        Session ses = sessionFactory.getCurrentSession();
        String hql = "from Offer order by " + sob + " " + oob;
        Query<Offer> thequery = ses.createQuery(hql, Offer.class);
        return thequery.getResultList();
    }

    @Override
    public List<Offer> getActiveOffers(Product product) {
        Session ses = sessionFactory.getCurrentSession();
        Query<Offer> thequery = ses.createQuery("from Offer where active=1 and product=:theProduct", Offer.class);
        thequery.setParameter("theProduct", product);
        return thequery.getResultList();
    }

    @Override
    public void saveOffer(Offer theOffer) {
        Session ses = sessionFactory.getCurrentSession();
        ses.saveOrUpdate(theOffer);
    }

    @Override
    public Offer getOffer(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        return ses.get(Offer.class, theId);
    }

    @Override
    public void deleteOffer(int theId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void activate(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("update Offer set active=1 where id=:offerId");
        thequery.setParameter("offerId", theId);
        thequery.executeUpdate();
    }
    
    @Override
    public void deactivate(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("update Offer set active=0 where id=:offerId");
        thequery.setParameter("offerId", theId);
        thequery.executeUpdate();
    }



}

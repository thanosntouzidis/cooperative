package com.cooperative.eshop.controller;

import com.cooperative.eshop.module.Category.CategoryService;
import com.cooperative.eshop.module.Customer.Customer;
import com.cooperative.eshop.module.Customer.CustomerService;
import com.cooperative.eshop.module.Member.Member;
import com.cooperative.eshop.module.Member.MemberService;
import com.cooperative.eshop.module.Offer.Offer;
import com.cooperative.eshop.module.Offer.OfferService;
import com.cooperative.eshop.module.Payment.Payment;
import com.cooperative.eshop.module.Payment.PaymentService;
import com.cooperative.eshop.module.Product.Product;
import com.cooperative.eshop.module.Product.ProductService;
import com.cooperative.eshop.module.Sale.Sale;
import com.cooperative.eshop.module.Sale.SaleService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/management-panel")
public class ManagementPanelController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SaleService saleService;
    @Autowired
    private OfferService offerService;
    @Autowired
    private PaymentService paymentService;

    @GetMapping
    public String showPanel(@RequestParam(name="salesBefore", defaultValue = "1", required=false) int sbef,

                            @RequestParam(name="sortProductsBy", defaultValue = "name") String sprb,
                            @RequestParam(name="sortCustomersBy", defaultValue = "username") String scb,
                            @RequestParam(name="sortMembersBy", defaultValue = "username") String smb,
                            @RequestParam(name="sortSalesBy", defaultValue = "dateofc") String ssb,
                            @RequestParam(name="sortOffersBy", defaultValue = "product.id") String sob,
                            @RequestParam(name="sortPaymentsBy", defaultValue = "date") String spab,

                            @RequestParam(name="orderProductsBy", defaultValue = "asc") String oprb,
                            @RequestParam(name="orderCustomersBy", defaultValue = "asc") String ocb,
                            @RequestParam(name="orderMembersBy", defaultValue = "asc") String omb,
                            @RequestParam(name="orderSalesBy", defaultValue = "desc") String osb,
                            @RequestParam(name="orderOffersBy", defaultValue = "asc") String oob,
                            @RequestParam(name="orderPaymentsBy", defaultValue = "desc") String opab,

                            Model model, Authentication au){

        Member currentMember = memberService.getMemberByUsername(au.getName());

        List<Product> products = productService.getProductsSortedBy(sprb, oprb);

        List<Customer> customers = customerService.getCustomersSortedBy(scb, ocb);

        List<Member> members = memberService.getMembersSortedBy(smb, omb);

        List<Sale> sales = saleService.getSalesSortedBy(ssb, osb);

        List<Offer> offers = offerService.getOffersSortedBy(sob, oob);

        List<Payment> payments = paymentService.getPaymentsSortedBy(spab, opab);



        model.addAttribute("customers", customers);
        model.addAttribute("members", members);
        model.addAttribute("products", products);
        model.addAttribute("sales", sales);
        model.addAttribute("offers", offers);
        model.addAttribute("payments", payments);
        model.addAttribute("ip", au.getName());
        Member mem = memberService.getMemberByUsername(au.getName());
        model.addAttribute("member", mem);

        model.addAttribute("sumSales", String.valueOf(saleService.getSalesSum()));
        model.addAttribute("adminProfit", String.valueOf(paymentService.getAdminProfit()));
        model.addAttribute("membersProfit", String.valueOf(paymentService.getMembersProfit()));
        model.addAttribute("personalProfit", String.valueOf(paymentService.getPersonalProfit(currentMember.getId())));

        model.addAttribute((sbef>1?"paginationMonth":"currentMonth"), (sbef>1?LocalDate.now().minusMonths(sbef-1).getMonth():LocalDate.now().getMonth()));

        String view = "management-panel";
        return view;
    }
    
    @GetMapping("/new-product")
    public String newProduct(ModelMap model){
        model.addAttribute("product", new Product());
        model.addAttribute("theCategories", categoryService.getCategories());
        return "product-form";
    }
    
    @GetMapping("/updateProduct")
    public String updateProduct(@RequestParam("productId") int theId, Model theModel) {
	Product theProduct = productService.getProduct(theId);
	theModel.addAttribute("product", theProduct);	
        theModel.addAttribute("theCategories", categoryService.getCategories());
	return "product-form";
    }
    
    @PostMapping("/save-product")
    public String submit(
            @Valid @ModelAttribute("product") Product theProduct,
            @RequestParam(required=false,name="myfile") MultipartFile file, ModelMap model,
            HttpServletRequest request) {
        
//        model.addAttribute("file", file);
        productService.saveProduct(theProduct);
        
        try {
            byte[] bytes = file.getBytes();
            String s = request.getContextPath();
            String s2 = (request.getContextPath() + "/resources/images/" + file.getOriginalFilename());
            model.addAttribute("info", s);
            model.addAttribute("info2", s2);
            Path path = Paths.get("//home//athan//Desktop//cooperativeeshop//src//main//webapp//resources//images//" + file.getOriginalFilename());
            Files.write(path, bytes);
        } catch (IOException ex) {
            Logger.getLogger(ManagementPanelController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "redirect:/management-panel";
    }
    
    
//    @PostMapping("/save-product")
//    public String submit(
//            @RequestParam("name") String name, 
//            @RequestParam("category_id") String category_id, 
//            @RequestParam("price_buy") Double price_buy,
//            @RequestParam("price_shop") Double price_shop,
//            @RequestParam("myfile") MultipartFile file, ModelMap model,
//            HttpServletRequest request) {
//        
//        model.addAttribute("file", file);
//        Product p = new Product(category_id, name, price_buy, price_shop);
//        productService.saveProduct(p);
//        
//        try {
//            byte[] bytes = file.getBytes();
//            String s = request.getRealPath("/");
//            String s2 = (request.getContextPath() + "/resources/images/" + file.getOriginalFilename());
//            model.addAttribute("info", s);
//            model.addAttribute("info2", s2);
//            Path path = Paths.get("//home//athan//Desktop//cooperativeeshop//src//main//webapp//resources//images//" + file.getOriginalFilename());
//            Files.write(path, bytes);
//        } catch (IOException ex) {
//            Logger.getLogger(ManagementPanelController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return "redirect:/shop";
//    }
    
    @GetMapping("/updateCustomer")
    public String showFormForUpdate(@RequestParam("customerId") int theId, Model theModel) {
	Customer theCustomer = customerService.getCustomer(theId);
	theModel.addAttribute("customer", theCustomer);		
	return "registration-form-customer";
    }
	
    @GetMapping("/deleteCustomer")
    public String deleteCustomer(@RequestParam("customerId") int theId) {
	customerService.deleteCustomer(theId);
	return "redirect:/shop";
    }
    
    
	
    @GetMapping("/deleteProduct")
    public String deleteProduct(@RequestParam("productId") int theId) {
	productService.deleteProduct(theId);
	return "redirect:/shop";
    }

    @GetMapping("/payment")
    public String getPayments(Model model){
        List<Payment> payments2 = paymentService.getPayments();
        model.addAttribute("payments2", payments2);
        return "management-panel";
    }
    
}
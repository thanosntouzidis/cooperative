package com.cooperative.eshop.module.Member;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MemberServiceImpl implements MemberService{
    
    @Autowired
    private MemberDAO memberDAO;
    
    
    @Override
    @Transactional
    public List<Member> getMembers(){
        return memberDAO.getMembers();
    }

    @Transactional
    @Override
    public void saveMember(Member theMember) {
        memberDAO.saveMember(theMember);
    }

    @Transactional
    @Override
    public Member getMember(int theId) {
        return memberDAO.getMember(theId);
    }
    
    @Transactional
    @Override
    public Member getMemberByUsername(String username) {
        return memberDAO.getMemberByUsername(username);
    }

    @Transactional
    @Override
    public void deleteMember(int theId) {
        memberDAO.deleteMember(theId);
    }

    @Transactional
    @Override
    public List<Member> searchMembers(String theSearchName) {
        return memberDAO.searchMembers(theSearchName);
    }

    @Transactional
    @Override
    public List<Member> getMembersSortedBy(String smb, String omb) {
        return memberDAO.getMembersSortedBy(smb, omb);
    }


}

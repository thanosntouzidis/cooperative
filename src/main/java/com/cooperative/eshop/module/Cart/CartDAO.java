package com.cooperative.eshop.module.Cart;

import com.cooperative.eshop.module.Customer.Customer;

import java.util.List;


public interface CartDAO {

    Cart getCartProduct(int theId);
    
    List<Cart> getProductsOfCart(Customer theCustomer);

    void saveProductToCart(Cart theCart);

    void deleteCartProduct(int theId);
    
}

package com.cooperative.eshop.module.Member;

import java.util.List;

public interface MemberDAO {
    
    List<Member> getMembers();

    void saveMember(Member theMember);

    Member getMember(int theId);

    void deleteMember(int theId);

    List<Member> searchMembers(String theSearchName);

    Member getMemberByUsername(String username);

    List<Member> getMembersSortedBy(String smb, String omb);
}

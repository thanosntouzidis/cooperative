package com.cooperative.eshop.module.Category;

import java.util.List;


public interface CategoryDAO {
    
    public List<Category> getCategories();
    
    public List<Category> getCategoryByName(String theName);

    public void saveCategory(Category theCategory);

    public Category getCategory(String theName);

    public void deleteCategory(String theName);
    
}

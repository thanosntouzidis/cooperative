CREATE DATABASE  IF NOT EXISTS `cooperativedb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `cooperativedb`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cooperativedb
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  UNIQUE KEY `authorities_idx_1` (`username`,`authority`),
  CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES ('athan','ROLE_ADMIN'),('melos','ROLE_MEMBER'),('melos2','ROLE_MEMBER'),('PanosA','ROLE_CUSTOMER'),('pelatakos','ROLE_CUSTOMER'),('pelaths','ROLE_CUSTOMER'),('pelaths10','ROLE_CUSTOMER'),('Pelaths5','ROLE_CUSTOMER'),('pennaki','ROLE_CUSTOMER'),('Takis','ROLE_CUSTOMER'),('vasilisW','ROLE_CUSTOMER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cart1_customer` (`customer`),
  KEY `fk_cart2_product` (`product`),
  CONSTRAINT `fk_cart1_customer` FOREIGN KEY (`customer`) REFERENCES `customer` (`id`),
  CONSTRAINT `fk_cart2_product` FOREIGN KEY (`product`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES ('fruits'),('mushrooms'),('vegetables');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_username_idx` (`username`),
  CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'pel','aths','pelaths@gmail.com','pelaths'),(2,'Billy','Billakos','vasilis@gmail.com','vasilisW'),(3,'Takaros','Takoulis','takis@gmail.com','Takis'),(4,'pela','ths','pelaths10@gmail.com','pelaths10'),(5,'Panos','Panoulis','panos@gmail.com','PanosA'),(7,'pela','takos','pelatakos@gmail.com','pelatakos'),(8,'penny','biri','penny@gmail.com','pennaki');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `ssn` varchar(45) DEFAULT NULL,
  `verified` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_member_1_idx` (`username`),
  KEY `index3` (`username`),
  CONSTRAINT `fk_member_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,'athan','Thanos','Ntouzidis','thanosntouzidis@gmail.com','12345',1),(3,'melos','oleeeme','patroklos','melos@gmail.com','12345',1),(4,'melos2','melos2','mel','melos2@gmail.com','12345',1);
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) DEFAULT NULL,
  `member` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product`),
  KEY `member_id` (`member`),
  CONSTRAINT `offers_ibfk_1` FOREIGN KEY (`product`) REFERENCES `product` (`id`),
  CONSTRAINT `offers_ibfk_2` FOREIGN KEY (`member`) REFERENCES `member` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (25,1,3,100,0),(26,1,4,100,1);
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale` int(11) DEFAULT NULL,
  `member` int(11) DEFAULT NULL,
  `amount` double DEFAULT '0',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_ibfk_1` (`sale`),
  KEY `payment_ibfk_2` (`member`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`sale`) REFERENCES `sales` (`id`),
  CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`member`) REFERENCES `member` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5060 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` VALUES (5054,5113,1,30,'2018-06-10 21:27:49'),(5055,5113,3,15,'2018-06-10 21:27:49'),(5056,5113,4,15,'2018-06-10 21:27:49'),(5057,5114,1,30,'2018-06-16 18:24:43'),(5058,5115,1,150,'2018-06-16 18:24:43'),(5059,5116,1,1,'2018-07-14 20:19:50');
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price_shop` double DEFAULT NULL,
  `price_buy` double DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `fk_product_1_idx` (`category`),
  CONSTRAINT `fk_product_1_fk` FOREIGN KEY (`category`) REFERENCES `categories` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'fruits','bananas',6,2,34,'mia perigrafh tou proiontos'),(2,'fruits','cherries',3,2,7,'mia perigrafh tou proiontos'),(3,'fruits','apricots',5,1,55,'mia perigrafh tou proiontos'),(4,'vegetables','artichokes',6,2,7,'mia perigrafh tou proiontos'),(6,'vegetables','cucumbers',2,2,785,'mia perigrafh tou proiontos'),(7,'fruits','grapes',1,8,755,'mia perigrafh tou proiontos'),(8,'mushrooms','portobello',6,2,1000,'mia perigrafh tou proiontos'),(9,'fruits','strawberries',3,2,98,'mia perigrafh tou proiontos'),(10,'fruits','watermelons',3,3,42,'mia perigrafh tou proiontos'),(11,'fruits','melons',6,2,41,'mia perigrafh tou proiontos'),(12,'vegetables','potatoes',4,5,500,NULL),(13,'vegetables','carrots',2,2,1000,'mia perigrafh tou proiontos'),(14,'vegetables','tomatoes',6,1,42,'mia perigrafh tou proiontos'),(15,'fruits','mangos',6,2,745,'mia perigrafh tou proiontos'),(17,'fruits','kiwis',3,2,17,'mia perigrafh tou proiontos'),(18,'mushrooms','creminis',6,2,42,'mia perigrafh tou proiontos'),(24,'mushrooms','cubensis',6,2,50,'mia perigrafh tou proiontos'),(25,'fruits','pineapples',6,2,1000,'mia perigrafh tou proiontos');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) DEFAULT NULL,
  `customer` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `paid` tinyint(4) DEFAULT '0',
  `completed` tinyint(4) DEFAULT '0',
  `dateofc` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product`),
  KEY `customer_id` (`customer`),
  CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`product`) REFERENCES `product` (`id`),
  CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`customer`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5117 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` VALUES (5113,1,1,6,10,1,0,'2018-06-10 21:27:49'),(5114,24,1,6,10,1,0,'2018-06-16 18:24:43'),(5115,2,1,3,100,1,0,'2018-06-16 18:24:43'),(5116,13,1,2,1,1,0,'2018-07-14 20:19:50');
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(75) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('athan','{bcrypt}$2a$04$wQpCWG/5679VUJxWjo6feuDcjrbV.k5CcOKlp7MXFHkQDtRxR4bia',1),('melos','{bcrypt}$2a$04$wQpCWG/5679VUJxWjo6feuDcjrbV.k5CcOKlp7MXFHkQDtRxR4bia',1),('melos2','{bcrypt}$2a$10$I4ScUXTlU04Nk1y4X9y6C.4a5ZzhvKV810I9khqdGlM1XzRKfbR9q',1),('PanosA','{bcrypt}$2a$10$ghCd6awPJX7nnYmE1sm3w.GWWf5Pug2hDFRRINQ7RKae44wwSTzv2',1),('pelatakos','{bcrypt}$2a$10$seaER08Ko5TgKgZASZTUrefUoPW3pwgKP54mHCrW4Emua5fLeNbAS',1),('pelaths','{bcrypt}$2a$10$r1EodzjJv3PEg84baMsZAuQnp0nW9RklGKxVrPaUyERjwfLttuOde',1),('pelaths10','{bcrypt}$2a$10$9diRsOXcMwRwvObjF8crie6ikdSl/OgpJlSh9wrIWeH.dbudgCsaa',1),('Pelaths5','{bcrypt}$2a$10$fP1O/GucLqZvl8QnyrBoGeYcUP1wDnbMIVmMAvGT9M.Kyrs7Bz5J.',1),('pennaki','{bcrypt}$2a$10$YcLBJb5Y/rRU9NOzXW.7IOHvoJQyoN9E4Fw1oM1v.4k0VipMiEhvC',1),('Takis','{bcrypt}$2a$10$NvzXhzX1M3uIf5Aa0ratHONpPxdRvCzwR8b80FJhQKOuOvRe1z1N6',1),('vasilisW','{bcrypt}$2a$10$BPCSdK0IVP7l5BhnbVwAr.o5zg8/qjpWOjMaWA4R2AuCBRxbCTLBO',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-15 14:58:51

package com.cooperative.eshop.module.Category;

import java.util.List;

public interface CategoryService {
    
    public List<Category> getCategories();
    
    public List<Category> getCategorysByName(String theName);
    
    public void saveCategory(Category theCategory);

    public Category getCategory(String theName);

    public void deleteCategory(String theName);
    
}

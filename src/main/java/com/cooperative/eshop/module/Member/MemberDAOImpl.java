package com.cooperative.eshop.module.Member;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDAOImpl implements MemberDAO{

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public List<Member> getMembers(){
        //get the current hibernate session
        Session ses = sessionFactory.getCurrentSession();
        
        //create a query
        Query<Member> thequery = ses.createQuery("from Member order by id", Member.class);
        
        //execute query and get result list
        List<Member> members = thequery.getResultList();
        
        return members; 
    }

    @Override
    public void saveMember(Member theMember) {
        Session ses = sessionFactory.getCurrentSession();
        ses.saveOrUpdate(theMember);
    }
    
    @Override
    public Member getMember(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        Member theMember = ses.get(Member.class, theId);
        return theMember;
    }

    @Override
    public List<Member> getMembersSortedBy(String smb, String omb) {
        Session ses = sessionFactory.getCurrentSession();
        String hql = "from Member order by " + smb + " " + omb;
        Query<Member> thequery = ses.createQuery(hql, Member.class);
        return thequery.getResultList();
    }
    
    @Override
    public Member getMemberByUsername(String username) {
        Session ses = sessionFactory.getCurrentSession();
        Query<Member> theQuery = ses.createQuery("from Member where username like :theName", Member.class);
        theQuery.setParameter("theName",username.toLowerCase());
        
        Member theMember = theQuery.getSingleResult();
        return theMember;
    }

    @Override
    public void deleteMember(int theId) {
        Session ses = sessionFactory.getCurrentSession();
        Query thequery = ses.createQuery("delete from Member where id=:menberId");
        thequery.setParameter("memberId", theId);
        thequery.executeUpdate();
    }

    @Override
    public List<Member> searchMembers(String theSearchName) {
        Session ses = sessionFactory.getCurrentSession();
        Query<Member> theQuery;
        if(theSearchName != null && theSearchName.trim().length()>0){
            theQuery = ses.createQuery("from Member where lower(firstName) like :theName or lower(lastName) like :theName", Member.class);
            theQuery.setParameter("theName","%"+theSearchName.toLowerCase()+"%");
        } else {
            theQuery = ses.createQuery("from Member", Member.class);
        }
        return theQuery.getResultList();
    }

    

   
}

    


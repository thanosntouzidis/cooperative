package com.cooperative.eshop.module.Payment;

import java.util.List;

/**
 *
 * @author Athan
 */
public interface PaymentDAO {

    void savePayment(Payment thePayment);

    List<Payment> getPayments();

    List<Object> getMembersPaymentAmounts();

    List<Object> getAdminPaymentAmounts();

    List<Object> getPersonalPaymentAmounts(int memberId);

    List<Payment> getPaymentsSortedBy(String spab, String opab);
}

package com.cooperative.eshop.module.Product;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductServiceImpl implements ProductService{
    
    @Autowired
    private ProductDAO productDAO;

    @Transactional
    @Override
    public Product getProduct(int theId) {
        return productDAO.getProduct(theId);
    }

    @Override
    @Transactional
    public List<Product> getProducts(){
        return productDAO.getProducts();
    }

    @Override
    public List<Product> getProductsScrollable() {
        return productDAO.getProductsScrollable();
    }

    @Override
    @Transactional
    public List<Product> getProductsByCategory(String theCategory) {
        return productDAO.getProductsByCategory(theCategory);
    }

    @Transactional
    @Override
    public List<Product> getProductsSortedBy(String sb, String or) {
        return productDAO.getProductsSortedBy(sb, or);
    }

    @Transactional
    @Override
    public void saveProduct(Product theProduct) {
        productDAO.saveProduct(theProduct);
    }

    @Transactional
    @Override
    public void deleteProduct(int theId) {
        productDAO.deleteProduct(theId);
    }




}

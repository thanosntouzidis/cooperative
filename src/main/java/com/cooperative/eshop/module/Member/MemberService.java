package com.cooperative.eshop.module.Member;

import java.util.List;

public interface MemberService {
    
    public List<Member> getMembers();
    
    public void saveMember(Member theMember);

    public Member getMember(int theId);
    
    public Member getMemberByUsername(String username);

    public void deleteMember(int theId);

    public List<Member> searchMembers(String theSearchName);

    List<Member> getMembersSortedBy(String smb, String omb);
}

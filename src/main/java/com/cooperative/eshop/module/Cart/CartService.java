package com.cooperative.eshop.module.Cart;

import com.cooperative.eshop.module.Customer.Customer;

import java.util.List;

public interface CartService {
    
    List<Cart> getProductsOfCart(Customer theCustomer);
    
    void saveProductToCart(Cart theCartProduct);

    Cart getCartProduct(int theId);

    void deleteCartProduct(int theId);
}

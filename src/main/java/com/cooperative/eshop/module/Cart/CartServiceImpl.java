package com.cooperative.eshop.module.Cart;

import java.util.List;

import com.cooperative.eshop.module.Customer.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CartServiceImpl implements CartService {
    
    @Autowired
    private CartDAO cartDAO;

    
    @Transactional
    @Override
    public List<Cart> getProductsOfCart(Customer theCustomer) {
        return cartDAO.getProductsOfCart(theCustomer);
    }
    
    @Transactional
    @Override
    public Cart getCartProduct(int theId) {
        return cartDAO.getCartProduct(theId);
    }

    @Transactional
    @Override
    public void deleteCartProduct(int theId) {
        cartDAO.deleteCartProduct(theId);
    }

    @Transactional
    @Override
    public void saveProductToCart(Cart theCartProduct) {
        cartDAO.saveProductToCart(theCartProduct);
    }

    
}

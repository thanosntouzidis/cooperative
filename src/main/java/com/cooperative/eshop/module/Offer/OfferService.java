package com.cooperative.eshop.module.Offer;

import com.cooperative.eshop.module.Product.Product;

import java.util.List;

public interface OfferService {
    
    List<Offer> getOffers();
    
    void saveOffer(Offer theOffer);

    Offer getOffer(int theId);

    void deleteOffer(int theId);
    
    void activate(int theId);
    
    void deactivate(int theId);

    List<Offer> getActiveOffers(Product product);

    List<Offer> getOffersSortedBy(String sob, String oob);
}

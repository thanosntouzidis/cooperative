
package com.cooperative.eshop.module.Customer;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomerServiceImpl implements CustomerService{
    
    @Autowired
    private CustomerDAO customerDAO;
    
    
    @Override
    @Transactional
    public List<Customer> getCustomers(){
        return customerDAO.getCustomers();
    }

    @Transactional
    @Override
    public void saveCustomer(Customer theCustomer) {
        customerDAO.saveCustomer(theCustomer);
    }

    @Transactional
    @Override
    public Customer getCustomer(int theId) {
        return customerDAO.getCustomer(theId);
    }
    
    @Transactional
    @Override
    public Customer getCustomerByUsername(String theUsername) {
        return customerDAO.getCustomerByUsername(theUsername);
    }

    @Transactional
    @Override
    public void deleteCustomer(int theId) {
        customerDAO.deleteCustomer(theId);
    }

    @Transactional
    @Override
    public List<Customer> searchCustomers(String theSearchName) {
        return customerDAO.searchCustomers(theSearchName);
    }

    @Transactional
    @Override
    public void disable(int theId) {
        customerDAO.disable(theId);
    }

    @Transactional
    @Override
    public List<Customer> getCustomersSortedBy(String scb, String ocb) {
        return customerDAO.getCustomersSortedBy(scb, ocb);
    }

}

package com.cooperative.eshop.module.Payment;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author Athan
 */
@Service
public class PaymentServiceImpl implements PaymentService {
    
    @Autowired
    private PaymentDAO paymentDAO;
    
    @Transactional
    @Override
    public void savePayment(Payment thePayment) {
        paymentDAO.savePayment(thePayment);
    }

    @Transactional
    @Override
    public List<Payment> getPayments() {
        return paymentDAO.getPayments();
    }

    @Transactional
    @Override
    public Double getAdminProfit() {
        Double theSum = 0.00;
        List<Object> list = paymentDAO.getAdminPaymentAmounts();
        for (Object o: list){
            theSum += ((Double)o);
        }
        return theSum;
    }

    @Transactional
    @Override
    public Double getMembersProfit() {
        Double theSum = 0.00;
        List<Object> list = paymentDAO.getMembersPaymentAmounts();
        for (Object o: list){
            theSum += ((Double)o);
        }
        return theSum;
    }

    @Transactional
    @Override
    public Double getPersonalProfit(int memberId) {
        Double theSum = 0.00;
        List<Object> list = paymentDAO.getPersonalPaymentAmounts(memberId);
        for (Object o: list){
            theSum += ((Double)o);
        }
        return theSum;
    }

    @Transactional
    @Override
    public List<Payment> getPaymentsSortedBy(String spab, String opab) {
        return  paymentDAO.getPaymentsSortedBy(spab, opab);
    }

}
